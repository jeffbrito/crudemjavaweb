<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import='Controle.UsuarioControle' %>
<!DOCTYPE html>
<html lang='pt-br en'>
<head>
		<meta charset="ISO-8859-1">
		<meta charset='utf-8'>
		<link rel='stylesheet' type='text/css' href='css/geral.css' />
<title>Crud</title>
</head>	

<body>
<% UsuarioControle control = new UsuarioControle(); %>


		<div class='form'>
			<form  action='Inserir' method='post'>
				<div class='inps'>
					<label for='user'>User</label>
					<input class='inpss' type='text' name='user' id='user' />
				</div>
				
				<div>
					<label for='email'>Email</label>
					<input class='inpss' type='text' name='email' id='email' />
				</div>
				
				<div>
					<label for='senha'>Senha</label>
					<input class='inpss' type='password' name='senha' id='senha' />
				</div>
				
				<div>
					<label for='conSenha'>Confirma��o de senha</label>
					<input class='inpss' type='password' name='conSenha' id='conSenha' />
				</div>
				
				<div>
					<input  type='submit' value='Enviar' />
				</div>
				
			</form>
		</div>
		
		<div class='table'>
		<%
							
			for(int i = 0;i < control.SelecionarTodos().size();i++){					
		%>
				<table>
						<tr>
								<th>Id</th>
								
								<th>Usu�rio</th>
								<th>Email</th>
								<th>senha</th>
								<th></th>
								<th></th>
						</tr>
						  <tr>
								<td><% int id = control.SelecionarTodos().get(i).getId(); %></td>
								<td><%out.print(id); %></td>
								<td><%out.print(control.SelecionarTodos().get(i).getUser()); %></td>
								<td><%out.print(control.SelecionarTodos().get(i).getEmail()); %></td>
								<td><%out.print(control.SelecionarTodos().get(i).getSenha()); %></td>
								<td><a href='UpdateUsuario.jsp'>Editar</a></td>
								<td><a href="delete?id=<%= id %>">Deletar</a></td>
						</tr>
					
				</table>
		</div>
	<% } %>		
</body>
</html>