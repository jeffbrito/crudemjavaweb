<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import='Controle.UsuarioControle' %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title></title>
</head>
<body>
<% UsuarioControle control = new UsuarioControle(); %>
<% 
	int i;
	for(i = 0;i < control.SelecionarTodos().size();i++){
		int id = control.SelecionarTodos().get(i).getId();
		String User = control.SelecionarTodos().get(i).getUser();
		String Email = control.SelecionarTodos().get(i).getEmail();
		String Senha = control.SelecionarTodos().get(i).getSenha();
%>
		<form action='Update?id=<%=id %>' method='post'>
				<div class='inpss'>
					<label for='user'>User</label>
					<input class='inpss' type='text' name='usuario' id='usuario' value='<%=User %>' />
				</div>
				
				<div>
					<label for='email'>Email</label>
					<input class='inpss' type='text' name='email' id='email' value='<%=Email %>' />
				</div>
				
				<div>
					<label for='senha'>Senha</label>
					<input class='inpss' type='password' name='senha' id='senha' value='<%=Senha %>' />
				</div>
				
				<div>
					<label for='conSenha'>Confirmação de senha</label>
					<input class='inpss' type='password' name='conSenha' id='conSenha' value='<%=Senha %>' />
				</div>
				
				<div>
					<input  type='submit' value='Enviar' />
				</div>
				
			</form>
		<% } %>
</body>
</html>