package Controle;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexao {
	
	private Connection conexao;
	public Conexao() {
		try {
			String host =  "localhost";
			String user = "root";
			String pwd = "";
			String bd = "crud";
			
			Class.forName("com.mysql.jdbc.Driver");
			this.setConexao(DriverManager.getConnection("jdbc:mysql://"+host+"/"+bd,user,pwd));
		}catch(SQLException e) {
			System.out.println("Erro de sql na classe Conexao:"+e.getMessage());
		}catch(Exception e) {
			System.out.println("Erro geral na classe Conexao:"+e.getMessage());
		}
	}
	public Connection getConexao() {
		return conexao;
	}
	public void setConexao(Connection conexao) {
		this.conexao = conexao;
	}
	public  void fecharConexao() {
		try {
			this.getConexao().close();
		}catch(SQLException e) {
			System.out.println("Erro de sql na classe Conexao:"+e.getMessage());
		}catch(Exception e) {
			System.out.println("Erro geral na classe Conexao:"+e.getMessage());
		}
	}

}
