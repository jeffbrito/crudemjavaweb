package Controle;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.sql.SQLException;
import Modelo.Usuario;
public class UsuarioControle {
	//Delete
	public boolean delete(int id) {
		try {
			Conexao con = new Conexao();
			String sql = "DELETE FROM usuario WHERE id = ?;";
			PreparedStatement ps = con.getConexao().prepareStatement(sql);
			ps.setInt(1,id);
			ps.executeUpdate();
		}catch(SQLException e) {
			System.out.println("Erro de SQL no delete:"+e.getMessage());
		}catch(Exception e) {
			System.out.println("Erro geral no delete:"+e.getMessage());
		}
		return true;
	}
	//Update
	public boolean UpdateUser(int id, Usuario user) {
		boolean retorno = false;
		try {
			Conexao con = new Conexao();
			String sql = "UPDATE usuario SET user = ?,email = ?, senha = ? WHERE id = ?;";
			PreparedStatement ps = con.getConexao().prepareStatement(sql);
			ps.setInt(4,id);
			ps.setString(1,user.getUser());
			ps.setString(2,user.getEmail());
			ps.setString(3,user.getSenha());
			if(!ps.execute()) {
				retorno = true;
			}
		}catch(SQLException e) {
			System.out.println("Erro de sql no updateDoUsuario:"+e.getMessage());
		}catch(Exception e) {
			System.out.println("Erro geral no Upadate:"+e.getMessage());
		}
		return retorno;
	}
	//Create
	public boolean insert(Usuario user) {
		boolean retorno = false;
		try {
			Conexao con = new Conexao();
			PreparedStatement ps = con.getConexao().prepareStatement("INSERT INTO usuario(user,email,senha) VALUES(?,?,?);");
			ps.setString(1,user.getUser());
			ps.setString(2,user.getEmail());
			ps.setString(3,user.getSenha());
			if(!ps.execute()) {
				retorno = true;
			}
			con.fecharConexao();
		}catch(SQLException e) {
			System.out.println("Erro de sql ao inserir Usu�rio"+e.getMessage());
			return false;
		}catch(Exception e){
			System.out.println("Erro geral ao inserir Usu�rio"+e.getMessage());
			return false;
		}
		return retorno;
	}
	//Read
	public ArrayList<Usuario> SelecionarTodos(){
		try {
			ArrayList<Usuario> lista = null;
			Conexao conexao =  new Conexao();
			String sql = "SELECT * FROM usuario;";
			PreparedStatement ps = conexao.getConexao().prepareStatement(sql);
			if(ps.execute()) {
				lista = new ArrayList<Usuario>();
					ResultSet rs = ps.executeQuery();
					while(rs.next()) {
						Usuario user = new Usuario();
						user.setId(rs.getInt("id"));
						user.setUser(rs.getString("user"));
						user.setSenha(rs.getString("senha"));
						user.setEmail(rs.getString("email"));
						lista.add(user);
						
					}
					conexao.fecharConexao();
					return lista;
				
			}else {
				return lista;
			}
		}catch(SQLException e) {
			System.out.println("Erro de sql na Controle:"+e.getMessage());
			return null;
		}catch(Exception e) {
			System.out.println("Erro geral:"+e.getMessage());
			return null;
		}
	}
}
